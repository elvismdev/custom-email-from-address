<?php
/*
Plugin Name: Custom Email From Address
Version: 0.1
Description: Modifies the Wordpress default "from email address" and "from name" used in an email sent using the wp_mail function.
Author: Elvis Morales
Author URI: https://twitter.com/n3rdh4ck3r
Plugin URI: https://bitbucket.org/grantcardone/custom-email-from-address
Text Domain: custom-email-from-address
*/
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


add_filter( 'wp_mail_from', function( $email ) {
	return 'info@grantcardonetv.com';
});

add_filter( 'wp_mail_from_name', function( $name ) {
	return 'GCTV LIVE';
});
